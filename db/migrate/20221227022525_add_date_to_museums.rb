class AddDateToMuseums < ActiveRecord::Migration[7.0]
  def change
    add_column :museums, :date, :date
  end
end
