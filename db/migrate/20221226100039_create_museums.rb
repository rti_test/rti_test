class CreateMuseums < ActiveRecord::Migration[7.0]
  def change
    create_table :museums do |t|
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
