Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "pages#home"
  # get "/", to: "pages#home"
  get "/musume", to: "musume#index"
  get "/musume/new", to: "musume#new", as:"new_mesume"
  get "/chartable_view", to: "chartable_view#index"

  post "/", to: "pages#home"

  # get "/musume/new", to: "resumes#new"
end
